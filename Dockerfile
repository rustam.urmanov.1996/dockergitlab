FROM node:18.13.0-alpine AS builder

WORKDIR /app

COPY package*.json .

RUN npm install --force

COPY . .

RUN npm run build --force

FROM nginx:1.21.3-alpine

COPY --from=builder /app/dist /usr/share/nginx/html 
COPY nginx.conf /etc/nginx/conf.d/default.conf

CMD ["nginx", "-g", "daemon off;"]